import pygatt.backends
import logging
import time
import datetime

def connect_device(adapter, address, retries=3):
    while retries > 0:
        try:
            device = adapter.connect(address, 8, pygatt.BLEAddressType.random)
            logging.info("Connected to device")
            return device
        except pygatt.exceptions.NotConnectedError as e:
            retries -= 1
            logging.warning(f"Connection attempt failed ({retries} retries left): {e}")
    logging.error("Failed to connect to the device after several attempts")
    return None

def wait_for_device(adapter):
    while True:
        try:
            if adapter.filtered_scan('1490BS'):
                logging.info("Found device")
                return True
        except pygatt.exceptions.BLEError as e:
            adapter.reset()
            logging.warning(f'BLE error: {e}')
            time.sleep(5)
    return False

def get_timestamp():
    now = datetime.datetime.now()
    timestamp = pack('<HBBBBBBxB', now.year, now.month, now.day, now.hour, now.minute, now.second, now.weekday() + 1, 1)
    return bytearray(timestamp)
