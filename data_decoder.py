from struct import unpack

def decode_blood_pressure(values):
    data = unpack('<BBxBxBxHBBBBBBxBxx', bytes(values[0:19]))
    return {
        "valid": data[0] == 0x1e,
        "systolic": data[1],
        "diastolic": data[2],
        "arterial": data[3],
        "year": str(data[4]),
        "month": str(data[5]).zfill(2),
        "day": str(data[6]).zfill(2),
        "hour": str(data[7]).zfill(2),
        "minute": str(data[8]).zfill(2),
        "second": str(data[9]).zfill(2),
        "pulse": data[10],
        "user": data[11],
    }
