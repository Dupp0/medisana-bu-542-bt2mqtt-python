import pygatt.backends
import logging
import time
import datetime
import json
from ble_device import connect_device, wait_for_device, get_timestamp
from mqtt_publisher import publish_mqtt
from data_decoder import decode_blood_pressure

# Load configuration from config.json
with open('config.json', 'r') as config_file:
    config = json.load(config_file)

def process_indication(handle, values):
    try:
        blood_pressure_data = decode_blood_pressure(values)
        if blood_pressure_data["valid"]:
            format_and_publish_message(blood_pressure_data)
    except Exception as e:
        logging.error(f"Error processing indication: {e}")

def format_and_publish_message(data):
    try:
        datetime_str = f"{data['year']}-{data['month']}-{data['day']}+{data['hour']}:{data['minute']}"
        mqtt_msg = f"{datetime_str},{data['systolic']},{data['diastolic']},{data['pulse']},{data['user']}"
        publish_mqtt(config['mqtt_broker'], config['mqtt_client_id'], config['mqtt_topic'], mqtt_msg)
    except Exception as e:
        logging.error(f"Error formatting message: {e}")

# Setup logging
logging.basicConfig(
    filename='/home/pi/bloodpressure/bloodPressure.log',
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S'
)
logger = logging.getLogger(__name__)
logging.getLogger('pygatt').setLevel(logging.CRITICAL)
logging.getLogger('paho.mqtt.client').setLevel(logging.WARNING)
logging.info("Starting up...")

adapter = pygatt.backends.GATTToolBackend()
adapter.start()

while True:
    try:
        if wait_for_device(adapter):
            device = connect_device(adapter, config['ble_device_address'])
            if not device:
                continue

            try:
                device.subscribe('00002a35-0000-1000-8000-00805f9b34fb', callback=process_indication, indication=True)
                device.char_write_handle(0x0024, get_timestamp(), wait_for_response=False)
            except pygatt.exceptions.NotificationTimeout as e:
                logging.warning(f"Notification timeout: {e}")
            except pygatt.exceptions.NotConnectedError as e:
                logging.warning(f"Lost connection during data transfer: {e}")
            
            time.sleep(5)
    except Exception as e:
        logging.error(f"Unexpected error in main loop: {e}")
