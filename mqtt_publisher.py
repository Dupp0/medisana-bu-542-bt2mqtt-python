import paho.mqtt.client as mqtt
import logging

def publish_mqtt(broker, client_id, topic, message):
    try:
        client = mqtt.Client(client_id)
        client.connect(broker)
        client.publish(topic, message)
        logging.info(f"Sent data to MQTT: {message}")
    except Exception as e:
        logging.error(f"Failed to publish to MQTT: {e}")
    finally:
        client.disconnect()
