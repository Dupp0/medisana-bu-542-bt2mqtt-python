# Medisana BU 542 BT2MQTT Python

## Overview

This project connects to a Medisana BU 542 blood pressure monitor, reads the data via Bluetooth, and publishes it to an MQTT broker.

## Features

- Connects to the Medisana BU 542 device via Bluetooth.
- Reads blood pressure measurements.
- Publishes data to an MQTT server.

## Requirements

- Python 3.x
- Required Python packages:
  - `paho-mqtt`
  - `pygatt[GATTTOOL]`

## Installation

1. Clone the repository:
    ```sh
    git clone https://github.com/suppoo/Medisana-BU-542-BT2MQTT-Python.git
    cd Medisana-BU-542-BT2MQTT-Python
    ```

2. Install the dependencies:
    ```sh
    pip install -r requirements.txt
    ```

3. Create and configure `config.json` with the following structure:
    ```json
    {
      "mqtt_broker": "127.0.0.1",
      "mqtt_client_id": "bloodpressure",
      "mqtt_topic": "monitor/bloodpressure",
      "ble_device_address": "FC:5A:C0:B7:D4:AE"
    }
    ```

## Usage

1. Run the main script:
    ```sh
    python main.py
    ```

## Configuration

- `config.json`: Modify the configuration file to update MQTT broker details, BLE device address, and other settings.

## Project Structure

- `main.py`: The main script to start the application.
- `ble_device.py`: Handles BLE device connection and data reading.
- `mqtt_publisher.py`: Manages MQTT client and message publishing.
- `data_decoder.py`: Decodes the raw data from the BLE device.

## Logging

Logs are stored in `/home/pi/bloodpressure/bloodPressure.log`. Adjust the logging settings in `main.py` as needed.

## Contributing

Please submit issues or pull requests for any bugs or improvements.

## License

This project is licensed under the MIT License. See the `LICENSE` file for details.
